//! This module defines the ```Session``` trait, which all sessions must implement, and
//! ```TypeMapSession```. ```TypeMapSession``` is a ```Session``` that has a ```TypeMap``` as its value.

use typemap::ShareMap;
use iron::typemap::Key;
use std::sync::{Arc, RwLock};

/// All ```Session``` objects that wish to be used in a ```SessionStore``` must implement this trait.
pub trait Session: Clone + Key + Send + Sync {

    /// Return a new empty instance
    fn empty() -> Self;

    /// Get the value that this session is storing
    fn get_value(&self) -> Self::Value;
}

/// A ```Session``` implementaion based on a ```TypeMap```. ```TypeMapSession``` is capable of storing many different
/// types of values and is useful as a general-purpose session.
#[derive(Clone)]
pub struct TypeMapSession {
    content: Arc<RwLock<ShareMap>>,
}

impl TypeMapSession {
    /// return a new empty ```TypeMapSession```
    pub fn new() -> TypeMapSession {
        let arc = Arc::new(RwLock::new(ShareMap::custom()));

        TypeMapSession{content: arc}
    }
}

impl Session for TypeMapSession {

    fn empty() -> TypeMapSession {
        Self::new()
    }

    fn get_value(&self) -> Self::Value {

        self.content.clone()
    }
}

impl Key for TypeMapSession {
    type Value = Arc<RwLock<ShareMap>>;
}
