#![warn(missing_docs)]
//! Session middleware for the [Iron](https://ironframework.io) web framework.
//!
//! The `Sessions` struct is used to create the new session middleware.
//! # Examples
//! Create and link a session:
//!
//! ```
//! # extern crate iron;
//! # extern crate iron_session;
//! #
//! # use iron_session::*;
//! # use iron::prelude::*;
//! #
//! # fn handler(req: &mut Request) -> IronResult<Response> {
//! #    Ok(Response::new())
//! # }
//! #
//! # fn main() {
//!     // create the chain that the session middleware will be linked in.
//!     let mut chain = Chain::new(handler);
//!
//!     // create the session middleware.
//!     let store: HashSessionStore<TypeMapSession> = HashSessionStore::new();
//!     chain.around(Sessions::new(String::from("secret").into_bytes(), store));
//! # }
//! ```
//!
//! Access the session data in a handler:
//!
//! ```
//! # extern crate iron;
//! # extern crate iron_session;
//! #
//! # use iron::prelude::*;
//! # use iron::status;
//! # use iron_session::*;
//! # use iron::typemap::Key;
//! // Create a key type that we can associate with a value type.
//! struct Counter;
//!
//! // implement the Key trait which associates u64 with Counter
//! impl Key for Counter { type Value = u64;}
//!
//! // iron request handler
//! fn create(req: &mut Request) -> IronResult<Response> {
//!
//!     // get the TypeMap for our session
//!     let lock = req.extensions.get::<TypeMapSession>().unwrap();
//!
//!     // we want to write to the map, so get a write lock on it.
//!     let mut map = lock.write().unwrap();
//!
//!     // if there was no counter object in the map, create one
//!     if let None = map.get::<Counter>() {
//!         map.insert::<Counter>(0);
//!     }
//!
//!     // get a mutable reference to the u64 inside the map
//!     let mut count = map.get_mut::<Counter>().unwrap();
//!     *count += 1;
//!
//!     // create a message with our hitcount and return in in a response
//!     let message = format!("hit count: {}", count);
//!     let mut res = Response::new();
//!
//!
//!     res.set_mut(status::Ok);
//!     res.set_mut(message);
//!     Ok(res)
//! }
//! # fn main() {}
//! ```

extern crate iron;
extern crate typemap;
extern crate oven;
extern crate cookie;
extern crate uuid;
extern crate time;

use std::marker::PhantomData;

use iron::{Chain, Request, Response, AfterMiddleware, BeforeMiddleware, AroundMiddleware, IronResult};
use iron::middleware;
use iron::typemap::Key;

pub mod session_store;
pub mod session;

use session_store::SessionStore;
use session::Session;

pub use session::TypeMapSession;
pub use session_store::hash_session::HashSessionStore;

use cookie::Cookie;
use oven::prelude::*;
use uuid::Uuid;

/// ```Sessions``` is a middleware to be used in a ```Chain```. It can handle any type of ```Session```
/// and ```SessionStore```
pub struct Sessions<T: Session, S: SessionStore<T>> {
    signing_key: Vec<u8>,
    store: S,
    phantom: PhantomData<T>
}

/// A ```Key``` type that is associated with the session's key.
pub struct SessionKey;

impl Key for SessionKey {type Value = Uuid;}

impl <T: Session, S: SessionStore<T>> Sessions<T, S> {
    /// Create a new ```Sessions``` middleware using the given store and signing_key. The signing_key
    /// shold be kept secret because it is used to sign and verify the session_id cookie.
    pub fn new(signing_key: Vec<u8>, store: S) -> Sessions<T, S> {
        Sessions{signing_key: signing_key, store: store, phantom: PhantomData}
    }

}

struct SessionsAfter;

impl AfterMiddleware for SessionsAfter {
    fn after(&self, req: &mut Request, mut res: Response) -> IronResult<Response> {
        let key = req.extensions.get::<SessionKey>().unwrap().clone();

        let mut cookie = req.get_cookie("session_key").map(|c| c.clone()).unwrap_or_else(|| {
            Cookie::new(String::from("session_key"), key.to_simple_string())
        });

        let exp = cookie.expires.unwrap_or(time::now_utc());

        if exp < time::now_utc() + time::Duration::minutes(20) {
            cookie.expires = Some(time::now_utc() + time::Duration::minutes(30));
            res.set_cookie(cookie);
        }

        Ok(res)
    }
}

struct SessionsBefore<T: Session, S: SessionStore<T>> {
    store: S,
    phantom: PhantomData<T>,

}

impl <T: Session, S: SessionStore<T>> BeforeMiddleware for SessionsBefore<T, S> {
    fn before(&self, req: &mut Request) -> IronResult<()> {
        let key = req.get_cookie("session_key").and_then(|cookie| Uuid::parse_str(&cookie.value).ok()).unwrap_or(self.store.gen_key());

        let val = match self.store.find(&key) {
            Some(val) => val,
            None => {
                let session = T::empty();
                self.store.insert(&key, session);
                self.store.find(&key).unwrap()
            }
        };

        req.extensions.insert::<T>(val.get_value());
        req.extensions.insert::<SessionKey>(key);

        Ok(())
    }
}

impl <T: Session, S: SessionStore<T>> AroundMiddleware for Sessions<T, S> {
    fn around(self, handler: Box<middleware::Handler>) -> Box<middleware::Handler> {
        let mut chain = Chain::new(handler);
        let key = self.signing_key;

        chain.link_after(SessionsAfter);
        chain.link(oven::new(key));
        chain.link_before(SessionsBefore{store: self.store, phantom: PhantomData});

        Box::new(chain)
    }
}
