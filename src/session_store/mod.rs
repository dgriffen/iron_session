//! This module defines the trait that all session stores need to implement.

pub mod hash_session;

use ::session::Session;
use uuid::Uuid;
use std::any::Any;

/// A ```SessionStore``` is an object capable of storing and retrieving multiple sessions.
pub trait SessionStore<V: Session>: Send + Sync + Any {

    /// Generate a new key for a session. Implementation is left open to the implementer in case
    /// they want to use a method that guarentees uniqueness.
    fn gen_key(&self) -> Uuid;

    /// Return the ```Session``` associated with the given key.
    fn find(&self, key: &Uuid) -> Option<V>;

    /// Insert the given ```Session``` into this store and associate it with the given key.
    fn insert(&self, key: &Uuid, val: V) -> ();
}
