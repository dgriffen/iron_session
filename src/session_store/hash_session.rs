//! This module contains the ```HashSessionStore```, a ```HashMap``` based ```SessionStore```.

use std::sync::{RwLock};
use std::collections::HashMap;
use super::SessionStore;
use uuid::Uuid;
use ::session::Session;

/// ```HashSessionStore``` is a ```SessionStore``` implementation based on a ```HashMap```.
/// ```HashSessionStore``` is capable of storing any type that implements ```Session```.
pub struct HashSessionStore<V: Session> {
    store: RwLock<HashMap<Uuid, V>>,
}

impl <V: Session> HashSessionStore<V> {
    /// Create a new empty ```HashSessionStore```
    pub fn new() -> HashSessionStore<V> {
        let map: HashMap<Uuid, V> = HashMap::new();
        let lock = RwLock::new(map);

        HashSessionStore{store: lock}
    }
}

impl <V: Session> SessionStore<V> for HashSessionStore<V> {

    fn gen_key(&self) -> Uuid {
        Uuid::new_v4()
    }

    fn find(&self, key: &Uuid) -> Option<V> {
        let lock: &RwLock<_> = &self.store;
        let map = lock.read().unwrap();
        map.get(key).map(|a| a.clone())
    }

    fn insert(&self, key: &Uuid, val: V) -> () {
        let lock: &RwLock<_> = &self.store;

        let mut map = lock.write().unwrap();
        map.insert(*key, val);
    }
}
